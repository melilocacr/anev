<?php global $pan_zoom; ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> ng-app="test">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo("title"); ?></title>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

    <style>
        @font-face {
            font-family: "Adobe Caslon Pro";
            src: url("<?php echo get_template_directory_uri(); ?>/fonts/Adobe Caslon Pro Bold.ttf");
        }
        @font-face {
            font-family: "Univers LT Std";
            src: url("<?php echo get_template_directory_uri(); ?>/fonts/univers-lt-std-55-roman.otf");
        }
        body {
            background: <?php echo $pan_zoom['background']; ?> !important;
        }
        .pan-zoom-frame {
            cursor: url("<?php echo get_template_directory_uri(); ?>/img/multidirectional.png"), auto;
        }
    </style>

    <?php wp_head(); ?>
</head>