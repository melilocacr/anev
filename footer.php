    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/hamsterjs/1.1.2/hamster.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.8/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-mousewheel/1.0.5/mousewheel.min.js"></script>

    <script src="<?php echo get_template_directory_uri(); ?>/js/default.js"></script>

    <script>
        $(document).ready(function(){
            $(".contact-info").hide();

            $(".info-btn").click(function() {
                $(".contact-info").show();
            });

            $(".close").click(function() {
                $(".contact-info").hide();
            });
        });
    </script>
    
    <?php wp_footer(); ?>
</body>
</html>