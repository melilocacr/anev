<?php get_header(); ?>

<body ng-controller="TestController" <?php body_class(); ?>>
    <main>
        <div class="container _panzoom">
            <div class="panzoom-wrapper">
                <panzoom id="PanZoom" class="panzoom" config="panzoomConfig" model="panzoomModel">
                    <div class="col-1">
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 5,
                                'offset'		=> 24,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 29,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 51,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                    </div>
                    <div class="col-2">
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 5,
                                'offset'		=> 14,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 31,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 49,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                    </div>
                    <div class="col-3">
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 5,
                                'offset'		=> 9,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 33,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 47,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                    </div>
                    <div class="col-4">
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,	
                            ));

                            while($_post->have_posts()) : 
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        
                        <div class="featured">
                            <img src="<?php echo esc_url( wp_get_attachment_url( get_theme_mod( 'custom_logo' ) ) ); ?>" alt="Featured Image">
                            
                            <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                        </div>

                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 2,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 35,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 45,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                    </div>
                    <div class="col-5">
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 5,
                                'offset'		=> 4,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 37,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 43,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                    </div>
                    <div class="col-6">
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 5,
                                'offset'		=> 19,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 39,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                        <?php
                            $_post = new WP_Query(array(
                                'post_type'		=> 'post',
                                'posts_per_page'=> 2,
                                'offset'		=> 41,	
                            ));

                            while($_post->have_posts()) :
                                $_post->the_post();
                                the_post_thumbnail();
                            endwhile;
                        ?>
                    </div>
                </panzoom>

                <div class="sidebar">
                    <!-- PanZoom Controller -->
                    <panzoomwidget panzoom-id="PanZoom" class="controller"></panzoomwidget>
                </div>

                <button class="info-btn">INFO.</button>
            </div>
            
            <div class="contact-info">
                <span class="close"><i class="fas fa-times"></i></span>
                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

                <div class="contact">
                    <p class="contact-text"><?php echo $pan_zoom['contact']; ?></p>
                    <span class="contact-title">Contact Us at</span>
                    <span class="email"><?php echo $pan_zoom['email']; ?></span>
                    <span class="phone"><?php echo $pan_zoom['phone']; ?></span>
                    <span class="address"><?php echo $pan_zoom['address']; ?></span>
                </div>

                <div class="social">
                    <a href="<?php echo $pan_zoom['facebook']; ?>"><i class="fab fa-facebook-f"></i></a>
                    <a href="<?php echo $pan_zoom['tiktok']; ?>"><i class="fab fa-tiktok"></i></a>
                    <a href="<?php echo $pan_zoom['twitter']; ?>"><i class="fab fa-twitter"></i></a>
                </div>
            </div>
        </div>
    </main>   

<?php get_footer(); ?>