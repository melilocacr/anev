<?php

function panzoom_setup() {

	add_theme_support( 'post-thumbnails' );
 
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'panzoom_setup' );

/**
* Enqueue scripts and styles.
*/

function panzoom_scripts() {
   wp_enqueue_style( 'panzoom-style', get_stylesheet_uri(), array(), _S_VERSION );
   wp_enqueue_style( 'panzoom-responsive', get_template_directory_uri() . '/css/responsive.css', array(), _S_VERSION );
}
add_action( 'wp_enqueue_scripts', 'panzoom_scripts' );

function admin_scripts() {
   wp_enqueue_style( 'admin', get_template_directory_uri() . '/css/admin.css');
}
add_action( 'admin_enqueue_scripts', 'admin_scripts' );

require_once('lib/redux-core/framework.php');
require_once('lib/sample/config.php');

?>